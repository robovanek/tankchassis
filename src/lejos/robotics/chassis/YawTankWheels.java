package lejos.robotics.chassis;

import lejos.robotics.RegulatedMotor;
import lejos.utility.Matrix;


/**
 * Wheels for tank with a yaw motor and correction motor
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class YawTankWheels {
    /**
     * Forward matrix.
     */
    private Matrix overall;
    /**
     * Differential (correction) side.
     */
    private RealTankWheel differential;
    /**
     * Turning (yaw) side.
     */
    private RealTankWheel turn;

    /**
     * Create new instance.
     *
     * @param diff Differential wheel.
     * @param turn Turning wheel.
     */
    public YawTankWheels(RealTankWheel diff, RealTankWheel turn) {
        this.differential = diff;
        this.turn = turn;
        calculateMatrix();
    }

    /**
     * Calculate forward matrix from tank wheels.
     */
    private void calculateMatrix() {
        if (differential.invert)
            differential.gearRatio *= -1;
        if (turn.invert)
            turn.gearRatio *= -1;

        overall = new Matrix(2, 3);
        final double deg2rad = Math.PI / 180.0D;

        double dmotor_const = 1D / (2D * differential.gearRatio);
        double dmotor_constV = 1D / differential.wheelRadius + 1D / turn.wheelRadius;
        double dmotor_constW = differential.offset / differential.wheelRadius + turn.offset / turn.wheelRadius;

        double tmotor_const = 1D / (turn.gearRatio * turn.wheelRadius);
        double tmotor_constV = 1;
        double tmotor_constW = -turn.offset;

        overall.set(0, 0, dmotor_const * dmotor_constV / deg2rad);
        overall.set(0, 1, 0D);
        overall.set(0, 2, dmotor_const * dmotor_constW);
        overall.set(1, 0, tmotor_const * tmotor_constV / deg2rad);
        overall.set(1, 1, 0D);
        overall.set(1, 2, tmotor_const * tmotor_constW);
    }

    /**
     * Get view of forward matrix.
     *
     * @return Wheels for {@link lejos.robotics.chassis.WheeledChassis}.
     */
    public Wheel[] getWheels() {
        return new Wheel[]{
                new FakeTankWheel(0, differential.motor),
                new FakeTankWheel(1, turn.motor)
        };
    }

    /**
     * Tank configurator
     */
    public static class RealTankWheel {
        /**
         * Driving motor.
         */
        private RegulatedMotor motor = null;
        /**
         * Gear ratio between motor and main axle.
         */
        private double gearRatio = 1D;
        /**
         * Radius of connected wheel.
         */
        private double wheelRadius;
        /**
         * Inversion of wheel drive.
         */
        private boolean invert = false;
        /**
         * Wheel offset.
         */
        private double offset = 0D;

        /**
         * Create a new tank wheel.
         *
         * @param motor         Driving motor.
         * @param wheelDiameter Connected wheel diameter.
         */
        public RealTankWheel(RegulatedMotor motor, double wheelDiameter) {
            this.motor = motor;
            this.wheelRadius = wheelDiameter / 2D;
        }

        /**
         * Set the gear ratio between the motor and the main axle.
         *
         * @param ratio Rotations of main axle divided by rotations of motor
         * @return This.
         */
        public RealTankWheel gearRatio(double ratio) {
            gearRatio = ratio;
            return this;
        }

        /**
         * Invert motor.
         *
         * @param really If the motor is inverted.
         * @return This.
         */
        public RealTankWheel invert(boolean really) {
            this.invert = really;
            return this;
        }

        /**
         * Set wheel offset.
         *
         * @param off Offset in units.
         * @return This.
         */
        public RealTankWheel offset(double off) {
            this.offset = off;
            return this;
        }
    }

    /**
     * Matrix projector
     */
    private class FakeTankWheel implements Wheel {
        /**
         * Projected row.
         */
        private int row;
        /**
         * Associated motor.
         */
        private RegulatedMotor motor;

        /**
         * Create new wheel matrix projection.
         *
         * @param row   Matrix row to project.
         * @param motor Motor of the wheel.
         */
        private FakeTankWheel(int row, RegulatedMotor motor) {
            this.row = row;
            this.motor = motor;
        }

        @Override
        public Matrix getFactors() {
            return overall.getMatrix(row, row, 0, 2);
        }

        @Override
        public RegulatedMotor getMotor() {
            return motor;
        }
    }
}
